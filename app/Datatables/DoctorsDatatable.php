<?php

namespace App\Datatables;

use App\Models\Boilerplate\User;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;

class DoctorsDatatable extends Datatable
{
    public $slug = 'doctors';

    public function datasource()
    {

        $query = User::query()->where('manager_id', Auth::user()->id);
        return $query;
    }

    public function setUp()
    {
        $this->order('created_at');
    }

    public function columns(): array
    {
        return [
            Column::add()
                ->width('40px')
                ->notSearchable()
                ->notOrderable()
                ->data('avatar', function ($user) {
                    return '<img src="' . $user->avatar_url . '" class="img-circle" width="32" height="32" />';
                }),
            Column::add(__('boilerplate::users.list.state'))
                ->width('100px')
                ->data('active', function ($user) {
                    $badge = '<span class="badge badge-pill badge-%s">%s</span>';
                    if ($user->active == 1) {
                        return sprintf($badge, 'success', __('boilerplate::users.active'));
                    }

                    return sprintf($badge, 'danger', __('boilerplate::users.inactive'));
                })
                ->filterOptions([__('boilerplate::users.inactive'), __('boilerplate::users.active')]),
            Column::add(__('boilerplate::users.lastname'))
                ->data('last_name'),
            Column::add(__('boilerplate::users.firstname'))
                ->data('first_name'),
            Column::add(__('boilerplate::users.email'))
                ->data('email'),
            Column::add(__('boilerplate::users.list.lastconnect'))
                ->width('12%')
                ->data('last_login')
                ->fromNow(),
            Column::add(__('boilerplate::users.list.creationdate'))
                ->width('180px')
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (User $user) {
                    return join([
                        // Button::show('plan.show', $plan),
                        Button::edit('boilerplate.doctors.edit', $user),
                        // Button::delete('boilerplate.plans.destroy', $plan),
                    ]);
                }),
        ];
    }
}
