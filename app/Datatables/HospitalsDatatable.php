<?php

namespace App\Datatables;

use App\Models\Boilerplate\User;
use App\Models\Client;
use App\Models\Hospital;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;
use Carbon\Carbon;


class HospitalsDatatable extends Datatable
{
    public $slug = 'hospitals';


    public function datasource()
    {
        return Hospital::query();
    }

    public function setUp()
    {
        $this->order('id', 'desc')
            // ->buttons('filters', 'csv', 'print')
            ->stateSave();
    }

    public function columns(): array
    {
        $columns = [
            Column::add(__('Name'))
                ->data('name'),

            Column::add(__('Created At'))
                ->width('180px')
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (Hospital $hospital) {
                    return join([
                        Button::edit('boilerplate.hospitals.edit', $hospital),
                    ]);
                }),
        ];

        return $columns;
    }
}
