<?php

namespace App\Datatables;

use App\Models\Boilerplate\User;
use App\Models\Meal;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;

class MealsDatatable extends Datatable
{
    public $slug = 'meals';

    public function datasource()
    {

        $query = Meal::query();
        return $query;
    }

    public function setUp()
    {
        $this->order('created_at');
    }

    public function columns(): array
    {
        return [

            Column::add(__('boilerplate::users.list.creationdate'))
                ->data('created_at', function ($meal) {
                    return $meal->created_at->format('d-m-Y');
                }),

            Column::add()
                ->width('20px')
                ->actions(function (Meal $meal) {
                    return join([
                        Button::edit('boilerplate.meals.edit', $meal),
                        Button::delete('boilerplate.meals.destroy', $meal),
                    ]);
                }),
        ];
    }
}
