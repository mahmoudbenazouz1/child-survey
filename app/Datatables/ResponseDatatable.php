<?php

namespace App\Datatables;

use App\Models\Submission;
use App\Models\Survey;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;


class ResponseDatatable extends Datatable
{
    public $slug = 'responses';


    public function datasource()
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            return Submission::query()->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->id);
                });
            });
        } elseif ($currentUser->hasRole('doctor')) {
            return Submission::query()->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->manager_id);
                });
            });
        }
    }

    public function setUp()
    {
        $this->order('id', 'desc')
            ->stateSave();
    }

    public function columns(): array
    {
        $columns = [

            Column::add(__('boilerplate::surveys.responses.child_name'))
                ->data('child_name'),

            Column::add(__('boilerplate::surveys.responses.survey'))
                ->data('survey_id', function ($submission) {
                    return @$submission->responses[0]->survey->name;
                }),

            Column::add(__('boilerplate::surveys.list.creationdate'))
                ->width('180px')
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (Submission $submission) {
                    return join([
                        Button::show('boilerplate.responses.edit', $submission),
                        Button::delete('boilerplate.responses.destroy', $submission)
                    ]);
                }),
        ];

        return $columns;
    }
}
