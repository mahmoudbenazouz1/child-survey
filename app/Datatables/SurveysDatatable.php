<?php

namespace App\Datatables;

use App\Models\Survey;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;


class SurveysDatatable extends Datatable
{
    public $slug = 'surveys';


    public function datasource()
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            return Survey::query()->where('manager_id', $currentUser->id);
        } elseif ($currentUser->hasRole('doctor')) {
            return Survey::query()->where('manager_id', $currentUser->manager_id);
        }
    }

    public function setUp()
    {
        $this->order('id', 'desc')
            // ->buttons('filters', 'csv', 'print')
            ->locale([
                'deleteConfirm' => __('boilerplate::surveys.list.confirmdelete'),
                'deleteSuccess' => __('boilerplate::surveys.list.deletesuccess'),
            ])
            ->stateSave();
    }

    public function columns(): array
    {
        $columns = [


            Column::add(__('boilerplate::surveys.status'))
                ->width('100px')
                ->data('status', function ($survey) {
                    $badge = '<span class="badge badge-pill badge-%s">%s</span>';
                    if ($survey->status == 1) {
                        return sprintf($badge, 'success', __('boilerplate::users.active'));
                    }

                    return sprintf($badge, 'danger', __('boilerplate::users.inactive'));
                })
                ->filterOptions([__('boilerplate::users.inactive'), __('boilerplate::users.active')]),

            Column::add(__('boilerplate::surveys.create.name'))
                ->data('name'),

            Column::add(__('boilerplate::surveys.list.creationdate'))
                ->width('180px')
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (Survey $survey) {
                    return join([
                        '<button class="btn btn-sm btn-info edit-button" data-toggle="modal" data-target="#editSurveyModal" data-id="' . $survey->id . '" data-toggle="tooltip" data-placement="top" ">',
                        '<i class="fa-regular fa-pen-to-square"></i>',
                        '</button>',
                        '<span class="button-space"></span>', // Space between buttons
                        Button::edit('boilerplate.surveys.edit', $survey),
                        Button::delete('boilerplate.surveys.destroy', $survey)
                    ]);
                }),
        ];

        return $columns;
    }
}
