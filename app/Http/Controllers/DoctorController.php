<?php

namespace App\Http\Controllers;

use App\Models\Boilerplate\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('doctors.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('doctors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'last_name'  => 'required',
            'first_name' => 'required',
            'email'      => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
        ]);

        $userModel = config('boilerplate.auth.providers.users.model');
        $user = $userModel::withTrashed()->updateOrCreate(['email' => $request->email], [
            'active' => $request->active,
            'hospital_id' => Auth::user()->hospital_id,
            'manager_id' => Auth::user()->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => bcrypt(Str::random(8)),
            'remember_token' => Str::random(32),
            'deleted_at' => null,
        ]);
        $user->restore();
        $user->roles()->sync([3]);

        $user->sendNewUserNotification($user->remember_token);

        return redirect()->route('boilerplate.doctors.edit', $user)
            ->with('growl', [__('boilerplate::users.successadd'), 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('doctor')) {
            if ($user->manager_id == Auth::user()->id) {
                return view('doctors.edit', [
                    'user' => $user,
                ]);
            }
            abort(403);
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update($id, Request $request)
    {
        $userModel = config('boilerplate.auth.providers.users.model');
        $user = $userModel::findOrFail($id);
        if ($user->hasRole(['doctor'])) {
            if ($user->manager_id == Auth::user()->id) {
                $this->validate($request, [
                    'last_name'  => 'required',
                    'first_name' => 'required',
                    'email'      => 'required|email|unique:users,email,' . $id,
                ]);

                $user->update([
                    'last_name'  => $request->last_name,
                    'first_name' =>  $request->first_name,
                    'email'      => $request->email,
                    'active' => $request->active,

                ]);


                return redirect()->route('boilerplate.doctors.edit', $user)
                    ->with('growl', [__('boilerplate::users.successmod'), 'success']);
            }
            abort(403);
        }
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
