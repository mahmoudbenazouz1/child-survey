<?php

namespace App\Http\Controllers;

use App\Http\Requests\HospitalRequest;
use App\Models\Hospital;
use App\Models\Meal;
use App\Models\Option;
use App\Models\Question;
use App\Models\Response;
use App\Models\Submission;
use App\Models\Survey;
use Illuminate\Http\Request;

class FrontOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('frontoffice.index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function games()
    {
        $surveys = Survey::where('status', '1')->whereHas('questions')->get();
        return view('frontoffice.games', [
            'surveys' => $surveys,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function meals()
    {
        $meals = Meal::all();
        return view('frontoffice.meals', [
            'meals' => $meals,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create($id)
    {
        $survey = Survey::findOrFail($id);
        return view('frontoffice.create', [
            'survey' => $survey,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'survey_id' => 'required|exists:surveys,id',
            'child_name' => 'required|string',
            'answers' => [
                'required',
                'array',
                function ($attribute, $value, $fail) use ($request) {
                    $surveyId = $request->input('survey_id');
                    $questionIds = Question::where('survey_id', $surveyId)->pluck('id')->toArray();

                    $keys = array_keys($value);
                    $uniqueKeys = array_unique($keys);

                    // Check if all keys are unique
                    if (count($keys) !== count($uniqueKeys)) {
                        $fail("The keys in the answers array must be unique.");
                        return;
                    }

                    // Check if all keys exist in the survey questions relation
                    foreach ($uniqueKeys as $key) {
                        if (!in_array($key, $questionIds)) {
                            $fail("Key $key does not exist in the survey questions relation.");
                            return;
                        }
                    }

                    // Check if each question has at least one option and no duplicate options
                    foreach ($value as $questionId => $options) {
                        if (!is_array($options) || count($options) < 1) {
                            $fail("Question $questionId must have at least one option.");
                            return;
                        }

                        if (count($options) !== count(array_unique($options))) {
                            $fail("Question $questionId should not have duplicate options.");
                            return;
                        }

                        $questionOptions = Option::where('question_id', $questionId)->pluck('id')->toArray();
                        foreach ($options as $optionId) {
                            if (!in_array($optionId, $questionOptions)) {
                                $fail("Option $optionId does not exist for question $questionId.");
                                return;
                            }
                        }
                    }
                },
            ],
        ]);

        $submission = new Submission();
        $submission->survey_id = $request->survey_id;
        $submission->child_name = $request->child_name;
        $submission->save();

        foreach ($request->answers as $questionId => $selectedOptions) {
            foreach ($selectedOptions as $optionId) {
                $response = new Response();
                $response->submission_id = $submission->id;
                $response->survey_id = $submission->survey_id;
                $response->question_id = $questionId;
                $response->option_id = $optionId;
                $response->save();
            }
        }

        return view('frontoffice.results', [
            'submission' => $submission,
            'survey' => $submission->survey,
            'answers' => $request->answers, // Passing submitted answers to the results page
        ]);
    }
}
