<?php

namespace App\Http\Controllers;

use App\Models\Submission;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('responses.list');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $submission = Submission::with(['responses', 'responses.question', 'responses.option'])->where('id', $id)->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->id);
                });
            })->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $submission = Submission::with(['responses', 'responses.question', 'responses.option'])->where('id', $id)->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->manager_id);
                });
            })->first();
        }


        if ($submission) {

            return view('responses.edit', [
                'submission' => $submission,
                'responses' => $submission->responses->groupBy('question_id'),
            ]);
        }
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $submission = Submission::where('id', $id)->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->id);
                });
            })->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $submission = Submission::where('id', $id)->whereHas('responses', function ($q) use ($currentUser) {
                $q->whereHas('survey', function ($q) use ($currentUser) {
                    $q->where('manager_id', $currentUser->manager_id);
                });
            })->first();
        }
        if ($submission) {

            return response()->json(['success' => $submission->delete() ?? false]);
        }
        abort(403);
    }
}
