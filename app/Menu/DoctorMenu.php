<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class DoctorMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        if (Auth::user()->hasRole('manager')) {
            $item = $menu->add('boilerplate::users.title', [
                'icon' => 'square',
                'order' => 1030,
            ]);

            $item->add('boilerplate::users.create_doctor.title', [
                'route' => 'boilerplate.doctors.create',
                'order' => 1002,
            ]);

            $item->add('boilerplate::users.list.title', [
                'route' => 'boilerplate.doctors.index',
                'active' => 'boilerplate.doctors.index,boilerplate.doctors.edit',
                'order' => 1003,
            ]);
        }
    }
}
