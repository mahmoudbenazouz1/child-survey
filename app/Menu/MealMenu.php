<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class MealMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        if (Auth::user()->hasRole(['manager', 'doctor'])) {
            $menu->add('boilerplate::meals.title', [
                'route' => 'boilerplate.meals.index',
                'active' => 'boilerplate.meals.index,boilerplate.meals.create,boilerplate.meals.edit',
                'order' => 1200,
            ]);
        }
    }
}
