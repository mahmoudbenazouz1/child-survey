<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class ResponseMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        if (Auth::user()->hasRole(['manager', 'doctor'])) {
            $menu->add('boilerplate::surveys.responses.title', [
                'route' => 'boilerplate.responses.index',
                'active' => 'boilerplate.responses.index,boilerplate.responses.edit',
                'order' => 1100,
            ]);
        }
    }
}
