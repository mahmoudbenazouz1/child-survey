<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class UserProfileMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
            $menu->add('boilerplate::users.profile.title', [
                'route' => 'boilerplate.user.profile',
                'order' => 998,
            ]);
    }
}
