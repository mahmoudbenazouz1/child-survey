<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    use HasFactory;
    protected $guarded = [];
    const BREAKFAST = 'BREAKFAST';
    const LUNCH = 'LUNCH';
    const DINNER = 'DINNER';

    public function meal()
    {
        return $this->belongsTo(Meal::class);
    }
}
