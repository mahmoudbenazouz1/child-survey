<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CorrectOptionsRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Custom validation logic for correct options count
        $correctOptionsCount = collect($value)->filter(function ($option) {
            return isset($option['correct']) && $option['correct'] === 'on';
        })->count();

        return $correctOptionsCount >= 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return 'At least one option must have the correct flag set to true.';
    }
}

