<?php

return [
    'default' => config('app.locale'),
    // 'switch' => true,
    'allowed' => ['en'],
    'languages' => [
        'en' => ['label' => 'English', 'datatable' => 'English'],
        // 'fr' => ['label' => 'Français', 'datatable' => 'French'],
        // 'ar' => ['label' => 'Arabe', 'datatable' => 'Arabic'],
    ],
];
