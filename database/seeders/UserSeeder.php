<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userModel = config('auth.providers.users.model');
        $roleModel = config('laratrust.models.role');

        $admin_user = $userModel::withTrashed()->updateOrCreate(['email' => 'admin@iec-telecom.com'], [
            'active'     => true,
            'first_name' => 'Admin',
            'last_name'  => 'Admin',
            'email'      => 'admin@admin.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $admin = $roleModel::whereName('admin')->first();
        $admin_user->attachRole($admin);

        $manager_user = $userModel::withTrashed()->updateOrCreate(['email' => 'manager@sahloul.com'], [
            'active'     => true,
            'first_name' => 'Manager',
            'last_name'  => 'Sahloul',
            'hospital_id'=> '1',
            'email'      => 'manager@sahloul.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $manager = $roleModel::whereName('manager')->first();
        $manager_user->attachRole($manager);

        $manager_user = $userModel::withTrashed()->updateOrCreate(['email' => 'manager@hachad.com'], [
            'active'     => true,
            'first_name' => 'Manager',
            'last_name'  => 'Hachad',
            'hospital_id'=> '2',
            'email'      => 'manager@hachad.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $manager_user->attachRole($manager);

        $doctor_user = $userModel::withTrashed()->updateOrCreate(['email' => 'doctor@sahloul.com'], [
            'active'     => true,
            'first_name' => 'Doctor',
            'last_name'  => 'Sahloul',
            'hospital_id'=> '1',
            'manager_id'=> '2',
            'email'      => 'doctor@sahloul.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $doctor = $roleModel::whereName('doctor')->first();
        $doctor_user->attachRole($doctor);

        $doctor_user = $userModel::withTrashed()->updateOrCreate(['email' => 'doctor@hachad.com'], [
            'active'     => true,
            'first_name' => 'Doctor',
            'last_name'  => 'hachad',
            'hospital_id'=> '2',
            'manager_id'=> '3',
            'email'      => 'doctor@hachad.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);
        $doctor_user->attachRole($doctor);

    }
}
