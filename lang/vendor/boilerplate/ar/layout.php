<?php

return [
    'direction'         => 'ltr',
    'mainmenu'          => 'Main menu',
    'online'            => 'Online',
    'logout'            => 'تسجيل خروج',
    'home'              => 'الصفحة الرئيسية',
    'dashboard'         => 'Dashboard',
    'access'            => 'Users',
    'role_management'   => 'Roles',
    'user_management'   => 'User management',
    'logoutconfirm'     => 'هل أنت متأكد أنك تريد تسجيل الخروج ؟',
    'rightsres'         => 'All rights reserved.',
    'darkmode'          => 'Dark mode',
    'fullscreen'        => 'Full screen',
    'stop_impersonate'  => 'Return to your profile',
    'view_as'           => 'View as user',
];
