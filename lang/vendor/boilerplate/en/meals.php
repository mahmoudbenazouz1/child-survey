<?php

return [
    'title'         => 'Meals',
    'returntolist'  => 'Meal list',
    'save'          => 'Save',
    'close'         => 'Close',
    'informations'  => 'Informations',
    'add_question'  => 'Add a question',
    'edit_question'  => 'Edit a question',
    'add_option'    => 'Add Option',
    'list_question' => 'Question list',
    'response_survey' => 'Response',
    'upload_files'  => 'Upload File',
    'upload_file'   => 'Upload File',
    'correct_option' => 'Meal type',
    'correct_option_1' => 'Breakfast',
    'correct_option_2' => 'Lunch',
    'correct_option_3' => 'Dinner',
    'option_name'   => 'Meal Name',
    'action'        => 'Action',
    'status'        => 'Status',
    'active'        => 'Enabled',
    'inactive'      => 'Disabled',
    'successadd'    => 'The meal has been correctly added.',
    'successmod'    => 'The meal has been correctly modified.',
    'create'        => [
        'title'     => 'Add a meal',
        'name'     => 'Name',
    ],
    'edit' => [
        'title'     => 'Edit a meal',
        'name'     => 'Name',
    ],
    'responses' => [
        'title'     => 'Responses',
        'child_name'     => 'Child name',
        'meal'     => 'Meal',
        'list' => [
            'title'         => 'Response list',
        ]
    ],
    'list' => [
        'title'         => 'Meal list',
        'id'            => 'Id',
        'state'         => 'Status',
        'creationdate'  => 'Creation date',
        'confirmdelete' => 'Do you confirm that you want to delete this meal ?',
        'deletesuccess' => 'The meal has been correctly deleted.',
        'deleteerror'   => 'An error occured when trying to delete the meal.',
    ],
];
