<?php

return [
    'title'         => 'Surveys',
    'returntolist'  => 'Survey list',
    'save'          => 'Save',
    'close'         => 'Close',
    'informations'  => 'Informations',
    'add_question'  => 'Add a question',
    'edit_question'  => 'Edit a question',
    'add_option'    => 'Add Option',
    'list_question' => 'Question list',
    'response_survey' => 'Response',
    'upload_files'  => 'Upload File',
    'upload_file'   => 'Upload File',
    'correct_option' => 'Correct Option',
    'option_name'   => 'Option Name',
    'action'        => 'Action',
    'status'        => 'Status',
    'active'        => 'Enabled',
    'inactive'      => 'Disabled',
    'successadd'    => 'The survey has been correctly added.',
    'successmod'    => 'The survey has been correctly modified.',
    'create'        => [
        'title'     => 'Add a survey',
        'name'     => 'Name',
    ],
    'edit' => [
        'title'     => 'Edit a survey',
        'name'     => 'Name',
    ],
    'responses' => [
        'title'     => 'Responses',
        'child_name'     => 'Child name',
        'survey'     => 'Survey',
        'list' => [
            'title'         => 'Response list',
        ]
    ],
    'list' => [
        'title'         => 'Survey list',
        'id'            => 'Id',
        'state'         => 'Status',
        'creationdate'  => 'Creation date',
        'confirmdelete' => 'Do you confirm that you want to delete this survey ?',
        'deletesuccess' => 'The survey has been correctly deleted.',
        'deleteerror'   => 'An error occured when trying to delete the survey.',
    ],
];
