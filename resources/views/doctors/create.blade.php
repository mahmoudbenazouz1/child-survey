@extends('boilerplate::layout.index', [
'title' => __('boilerplate::users.title'),
'subtitle' => __('boilerplate::users.create_doctor.title'),
'breadcrumb' => [
__('boilerplate::users.title') => 'boilerplate.doctors.index',
__('boilerplate::users.create_doctor.title')
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.doctors.store') }}" autocomplete="off">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.doctors.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::doctors.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::users.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::users.informations'])
            @component('boilerplate::select2', ['name' => 'active', 'label' => 'boilerplate::users.status', 'minimum-results-for-search' => '-1'])
            <option value="1" @if (old('active', 1)=='1' ) selected="selected" @endif>@lang('boilerplate::users.active')</option>
            <option value="0" @if (old('active')=='0' ) selected="selected" @endif>@lang('boilerplate::users.inactive')</option>
            @endcomponent
            <div class="row">
                <div class="col-md-6 col-lg-12 col-xl-6">
                    @component('boilerplate::input', ['name' => 'first_name', 'label' => 'boilerplate::users.firstname', 'autofocus' => true])@endcomponent
                </div>
                <div class="col-md-6 col-lg-12 col-xl-6">
                    @component('boilerplate::input', ['name' => 'last_name', 'label' => 'boilerplate::users.lastname'])@endcomponent
                </div>
            </div>
            @component('boilerplate::input', ['name' => 'email', 'label' => 'boilerplate::users.email', 'help' => 'boilerplate::users.create.help'])@endcomponent
            @endcomponent
        </div>
    </div>
</form>
@endsection