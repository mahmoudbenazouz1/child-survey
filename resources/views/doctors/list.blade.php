@extends('boilerplate::layout.index', [
'title' => __('boilerplate::users.title'),
'subtitle' => __('boilerplate::users.list.title'),
'breadcrumb' => [
__('boilerplate::users.list.title') => 'boilerplate.doctors.index'
]
])

@section('content')
<div class="row">
    <div class="col-12 mbl">
        <span class="float-right pb-3">
            <a href="{{ route("boilerplate.doctors.create") }}" class="btn btn-primary">
                @lang('boilerplate::users.create_doctor.title')
            </a>
        </span>
    </div>
</div>

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'doctors']) @endcomponent

@endcomponent
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush