<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$survey->name}} | {{env('APP_NAME')}}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-image: url('{{ asset("assets/background/02.jpeg") }}');
            background-size: cover;
            background-position: center;
        }

        .container {
            margin: 50px 10px;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .question {
            display: none;
        }

        .question.active {
            display: block;
        }

        .question h5 {
            margin-bottom: 10px;
        }

        .options {
            margin-bottom: 20px;
        }

        .option {
            display: block;
            margin-bottom: 10px;
        }

        .option input[type="checkbox"] {
            display: none;
        }

        .option label {
            display: flex;
            align-items: center;
            cursor: pointer;
            padding: 8px;
            border-radius: 5px;
            border: 2px solid #ccc;
            transition: all 0.3s ease;
        }

        .option label:hover {
            background-color: #f0f0f0;
        }

        .option input[type="checkbox"]:checked+label {
            border-color: #007bff;
            background-color: #e7f3ff;
        }

        .option img {
            margin-right: 10px;
            width: 200px;
            /* Adjust image width */
            height: auto;
            object-fit: cover;
            border-radius: 0;
            /* Remove border-radius for square images */
        }

        .btn-group {
            margin-top: 20px;
            text-align: center;
        }

        .btn {
            padding: 8px 16px;
            margin-right: 10px;
        }

        .back-btn {
            display: inline-block;
            padding: 8px 16px;
            margin-bottom: 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        .back-btn:hover {
            background-color: #0056b3;
        }

        .alert.alert-danger {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            padding: 0.75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
        }

        .alert.alert-danger ul {
            margin-bottom: 0;
            padding-left: 1.25rem;
        }

        .alert.alert-danger li {
            list-style-type: disc;
            margin-left: 0.5rem;
        }
    </style>
</head>

<body>

    <div class="container">
        <a href="{{ url('/front-games') }}" class="back-btn"><i class="fas fa-arrow-left"></i></a>

        <h1>{{$survey->name}}</h1>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div id="questionNumber"></div>
        <form action="{{ route('frontoffice.store') }}" method="POST" id="surveyForm">
            @csrf
            <input type="hidden" name="survey_id" value="{{ $survey->id }}">
            <div id="questionContainer">
                @foreach($survey->questions as $index => $question)
                <div class="question{{ $index === 0 ? ' active' : '' }}">
                    <h5>{{ $question->name }}</h5>
                    <div class="options">
                        @foreach($question->options as $option)
                        <div class="option">
                            <input type="checkbox" name="answers[{{ $question->id }}][]" value="{{ $option->id }}" id="option{{ $option->id }}">
                            <label for="option{{ $option->id }}">
                                @if($option->image)
                                <img src="{{ asset($option->image) }}" alt="Option Image">
                                @endif
                                {{ $option->text }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            <div class="btn-group">
                <button type="button" id="prevQuestion" class="btn btn-secondary" style="display: none;">السابق</button>
                <button type="button" id="nextQuestion" class="btn btn-primary">التالي</button>
                <button type="submit" id="submitForm" class="btn btn-success" style="display: none;">إرسال</button>
            </div>
            <button type="submit" class="btn btn-success" style="display: none;">إرسال</button>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            let currentQuestion = 0;
            const totalQuestions = $('.question').length;

            function showQuestion(questionNumber) {
                $('.question').removeClass('active');
                $(`.question:eq(${questionNumber})`).addClass('active');

                // Show/hide Previous button based on current question number
                if (currentQuestion === 0) {
                    $('#prevQuestion').hide();
                } else {
                    $('#prevQuestion').show();
                }

                // Show/hide Next button and Submit button based on current question number
                if (currentQuestion === totalQuestions - 1) {
                    $('#nextQuestion').hide();
                    $('#submitForm').show();
                } else {
                    $('#nextQuestion').show();
                    $('#submitForm').hide();
                }

                // Update question number display
                $('#questionNumber').text(`${currentQuestion + 1}/${totalQuestions}`);
            }

            $('#nextQuestion').click(function() {
                const checkboxes = $(`.question:eq(${currentQuestion}) input[type="checkbox"]:checked`);
                if (checkboxes.length > 0) {
                    if (currentQuestion < totalQuestions - 1) {
                        currentQuestion++;
                        showQuestion(currentQuestion);
                    }
                } else {
                    alert('الرجاء تحديد خيار واحد على الأقل قبل المتابعة.');
                }
            });

            $('#prevQuestion').click(function() {
                if (currentQuestion > 0) {
                    currentQuestion--;
                    showQuestion(currentQuestion);
                }
            });

            $('#surveyForm').submit(function(e) {
                const checkboxes = $(`.question:eq(${currentQuestion}) input[type="checkbox"]:checked`);
                if (checkboxes.length === 0) {
                    alert('يرجى تحديد خيار واحد على الأقل قبل الإرسال.');
                    e.preventDefault(); // Prevent form submission
                } else {
                    if (currentQuestion === totalQuestions - 1) {
                        e.preventDefault(); // Prevent form submission
                        const childName = prompt('أدخل اسم الطفل:');
                        if (childName && childName.trim() !== '') {
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'child_name',
                                value: childName.trim()
                            }).appendTo('#surveyForm');
                            $('#surveyForm').unbind('submit').submit(); // Submit the form with child name
                        } else {
                            alert('الرجاء إدخال اسم الطفل.');
                        }
                    }
                }
            });

            showQuestion(currentQuestion); // Show the first question initially
        });
    </script>

</body>

</html>