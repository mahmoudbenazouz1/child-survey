@extends('boilerplate::auth.layout', ['title' => __('boilerplate::games.m_title')])

@section('content')
<style>
    body {
        background-image: url('{{ asset("assets/background/02.jpeg") }}');
        background-size: cover;
        background-position: center;
    }
    .survey-card {
        height: 100px;
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        padding: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: border-color 0.3s ease;
        text-align: center;
        /* Center text horizontally */
    }

    .survey-card:hover {
        border: 2px solid #28a745;
    }
    .survey-card:hover a {
        color: #28a745;
    }

    .survey-card .card-title {
        margin-bottom: 0;
    }

    .survey-link {
        text-decoration: none;
        color: inherit;
        /* Inherit text color from parent */
        width: 100%;
        /* Ensure the link takes up the entire card */
        height: 100%;
        /* Ensure the link takes up the entire card */
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .back-btn {
        display: inline-block;
        padding: 8px 16px;
        margin-bottom: 20px;
        background-color: #28a745;
        color: #fff;
        text-decoration: none;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .back-btn:hover {
        background-color: #0d6b26;
        color: #fff;
    }
    .container {
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.5);
        padding: 20px;
        background:rgb(248 248 235 / 70%);
        border-radius: 10px;
    }
</style>

<div class="container mt-5">
    <a href="{{ url('/') }}" class="back-btn"><i class="fas fa-arrow-left"></i></a>
    <div class="row justify-content-center">
        @foreach($meals as $meal)
        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
            <div class="card survey-card" id="meal_{{ $meal->id }}">
                <a href="#" class="survey-link" data-toggle="modal" data-target="#mealModal_{{ $meal->id }}">
                    <h5 class="card-title">وجبة يوم {{ \Carbon\Carbon::parse($meal->created_at)->locale('ar')->isoFormat('dddd D MMMM YYYY') }}</h5>
                </a>
            </div>
        </div>
        <div class="modal fade" id="mealModal_{{ $meal->id }}" tabindex="-1" role="dialog" aria-labelledby="mealModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mealModalLabel">وجبة يوم {{ \Carbon\Carbon::parse($meal->created_at)->locale('ar')->isoFormat('dddd D MMMM YYYY') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @foreach($meal->suggestions->groupBy('type') as $type => $suggestions)
                        <div class="accordion" id="accordion-{{ $type }}">
                            <div class="card">
                                <div class="card-header" id="heading-{{ $type }}" data-toggle="collapse" data-target="#collapse-{{ $type }}" aria-expanded="true" aria-controls="collapse-{{ $type }}" style="color: #28a745;">
                                    <h6 class="mb-0">
                                        @if($type == 'BREAKFAST')
                                        وجبة الفطور
                                        @elseif($type == 'LUNCH')
                                        وجبة الغداء
                                        @elseif($type == 'DINNER')
                                        وجبة العشاء
                                        @endif
                                    </h6>
                                </div>
                                <div id="collapse-{{ $type }}" class="collapse" aria-labelledby="heading-{{ $type }}" data-parent="#accordion-{{ $type }}">
                                    <div class="card-body">
                                        <div class="row">
                                            @foreach($suggestions as $suggestion)
                                            <div class="col-md-12 mb-4">
                                                <div class="card">
                                                    <img src="{{ asset($suggestion['image']) }}" alt="{{ $suggestion['text'] }}" class="card-img-top">
                                                    <div class="card-body">
                                                        <p class="card-text">{{ $suggestion['text'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلق</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection