<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-image: url('{{ asset("assets/background/02.jpeg") }}');
            background-size: cover;
            background-position: center;
        }

        .container {
            max-width: 800px;
            margin: 50px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .option {
            display: block;
            margin-bottom: 10px;
        }

        .option label {
            display: flex;
            align-items: center;
            cursor: default;
            padding: 8px;
            border-radius: 5px;
            border: 2px solid transparent;
            transition: all 0.3s ease;
        }

        .option.correct label {
            background-color: #c8e6c9;
        }

        .option.incorrect label {
            background-color: #ffcdd2;
        }

        .option.missing label {
            background-color: #fff9c4;
        }

        .option img {
            margin-right: 10px;
            width: 100px;
            height: auto;
            object-fit: cover;
            border-radius: 0;
        }

        .btn-group {
            margin-top: 20px;
            text-align: center;
        }

        .btn {
            padding: 8px 16px;
            margin-right: 10px;
        }

        .back-btn {
            display: inline-block;
            padding: 8px 16px;
            margin-bottom: 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        .back-btn:hover {
            background-color: #0056b3;
        }

        .question-name {
            margin-bottom: 10px;
        }

        .result-list {
            list-style-type: none;
            padding: 0;
        }

        .result-item {
            margin-bottom: 10px;
        }

        /* Additional styling for correct/incorrect options */
        .option.correct label:hover {
            background-color: #c8e6c9;
            /* Green hover effect for correct options */
        }

        .option.incorrect label:hover {
            background-color: #ffcdd2;
            /* Red hover effect for incorrect options */
        }

        .option.missing label:hover {
            background-color: #fff9c4;
            /* Yellow hover effect for missing options */
        }

        /* Hover effect for Back to Home button */
        .btn-group .btn:hover {
            background-color: #0056b3;
        }

        .option.correct label {
            background-color: #c8e6c9;
            border: 2px solid #28a745;
            /* Green border for correct options */
        }

        .option.incorrect label {
            background-color: #ffcdd2;
            border: 2px solid #dc3545;
            /* Red border for incorrect options */
        }

        .option.missing label {
            background-color: #fff9c4;
            border: 2px solid #ffc107;
            /* Yellow border for missing options */
        }

        .option label {
            display: flex;
            align-items: center;
            cursor: default;
            padding: 8px;
            border-radius: 5px;
            border: 2px solid transparent;
            transition: all 0.3s ease;
            border-color: #ccc;
            /* Default border color */
        }   
    </style>
</head>

<body>

    <div class="container">
        <a href="{{ url('/front-games') }}" class="back-btn"><i class="fas fa-arrow-left"></i></a>

        <h1>{{ $survey->name }} نتائج</h1>

        <h2>{{ $submission->child_name }} : أرسل من قبل</h2>

        @foreach($survey->questions as $question)
        <div class="question">
            <h3 class="question-name">{{ $question->name }}</h3>
            <ul class="result-list">
                @foreach($question->options as $option)
                <li class="result-item option
        @if(in_array($option->id, $answers[$question->id] ?? []))
            @if($option->correct_answer == 'on')
                correct
            @else
                incorrect
            @endif
        @else
            @if ($option->correct_answer == 'on')
                missing
            @endif
        @endif
    ">
                    <label>
                        @if($option->image)
                        <img src="{{ asset($option->image) }}" alt="Option Image">
                        @endif
                        {{ $option->text }}
                    </label>
                </li>
                @endforeach
            </ul>
        </div>
        @endforeach
    </div>

</body>

</html>