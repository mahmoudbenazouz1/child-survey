@extends('boilerplate::layout.index', [
'title' => __('boilerplate::hospitals.title'),
'subtitle' => __('boilerplate::hospitals.create.title'),
'breadcrumb' => [
__('boilerplate::hospitals.title') => 'boilerplate.hospitals.index',
__('boilerplate::hospitals.create.title')
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.hospitals.store') }}" autocomplete="off">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.hospitals.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::hospitals.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::hospitals.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::hospitals.informations'])
            @component('boilerplate::input', ['name' => 'name', 'label' => 'boilerplate::hospitals.name'])@endcomponent
            @endcomponent
        </div>
    </div>
</form>
@endsection