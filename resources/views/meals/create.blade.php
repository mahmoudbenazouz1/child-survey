@extends('boilerplate::layout.index', [
'title' => __('boilerplate::meals.title'),
'subtitle' => __('boilerplate::meals.create.title'),
'breadcrumb' => [
__('boilerplate::meals.title') => 'boilerplate.meals.index',
__('boilerplate::meals.create.title')
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.meals.store') }}" autocomplete="off" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.meals.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::meals.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::meals.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::meals.informations'])
            @if($errors->any())
            <div class="alert alert-danger">
                @php
                $displayedErrors = [];
                @endphp
                @foreach ($errors->all() as $error)
                @if (!in_array($error, $displayedErrors) && strpos($error, 'name') !== 0)
                <div>{{ $error }}</div>
                @php
                $displayedErrors[] = $error;
                @endphp
                @endif
                @endforeach
            </div>
            @endif
            <label for="date">Date</label>
            <input type="date" name="date" id="date" class="form-control">
            <p><button type="button" class="btn btn-info mt-3" id="add-option-btn">@lang('boilerplate::meals.add_option')</button></p>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="options" role="tabpanel" aria-labelledby="options-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="options-table">
                            <thead>
                                <tr>
                                    <th>@lang('boilerplate::meals.upload_files')</th>
                                    <th>@lang('boilerplate::meals.correct_option')</th>
                                    <th>@lang('boilerplate::meals.option_name')</th>
                                    <th>@lang('boilerplate::meals.action')</th>
                                </tr>
                            </thead>
                            <tbody id="options-body">
                                <!-- First row -->
                                <tr class="option-row">
                                    <td>
                                        <label for="file_1">@lang('boilerplate::meals.upload_file')</label>
                                        <input type="file" id="file_1" name="options[1][file]">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_1_1" name="options[1][correct]" value="1">
                                            <label class="form-check-label" for="correct_1_1">@lang('boilerplate::meals.correct_option_1')</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_1_2" name="options[1][correct]" value="2">
                                            <label class="form-check-label" for="correct_1_2">@lang('boilerplate::meals.correct_option_2')</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_1_3" name="options[1][correct]" value="3">
                                            <label class="form-check-label" for="correct_1_3">@lang('boilerplate::meals.correct_option_3')</label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="option_name_1" name="options[1][option_name]" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-option">Delete</button>
                                    </td>
                                </tr>
                                <!-- Second row -->
                                <tr class="option-row">
                                    <td>
                                        <label for="file_2">@lang('boilerplate::surveys.upload_files')</label>
                                        <input type="file" id="file_2" name="options[2][file]">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_2_1" name="options[2][correct]" value="1">
                                            <label class="form-check-label" for="correct_2_1">@lang('boilerplate::meals.correct_option_1')</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_2_2" name="options[2][correct]" value="2">
                                            <label class="form-check-label" for="correct_2_2">@lang('boilerplate::meals.correct_option_2')</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" id="correct_2_3" name="options[2][correct]" value="3">
                                            <label class="form-check-label" for="correct_2_3">@lang('boilerplate::meals.correct_option_3')</label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="option_name_2" name="options[2][option_name]" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-option">Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endcomponent
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        // Counter for unique IDs
        let optionCounter = 2;

        // Function to add a new option row to the table
        function addOptionRow() {
            optionCounter++;

            let optionRow = `<tr class="option-row">
    <td>
        <label for="file_${optionCounter}">@lang('boilerplate::surveys.upload_file')</label>
        <input type="file" id="file_${optionCounter}" name="options[${optionCounter}][file]">
    </td>
    <td>
        <div class="form-check">
            <input class="form-check-input" type="radio" id="correct_${optionCounter}_1" name="options[${optionCounter}][correct]" value="1">
            <label class="form-check-label" for="correct_${optionCounter}_1">@lang('boilerplate::meals.correct_option_1')</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" id="correct_${optionCounter}_2" name="options[${optionCounter}][correct]" value="2">
            <label class="form-check-label" for="correct_${optionCounter}_2">@lang('boilerplate::meals.correct_option_2')</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" id="correct_${optionCounter}_3" name="options[${optionCounter}][correct]" value="3">
            <label class="form-check-label" for="correct_${optionCounter}_3">@lang('boilerplate::meals.correct_option_3')</label>
        </div>
    </td>
    <td>
        <input type="text" id="option_name_${optionCounter}" name="options[${optionCounter}][option_name]" class="form-control">
    </td>
    <td>
        <button type="button" class="btn btn-danger delete-option">Delete</button>
    </td>
    </tr>`;

            $('#options-body').append(optionRow);
        }

        // Add option button click event
        $('#add-option-btn').click(function() {
            addOptionRow();
        });

        // Delete option button click event (delegated to handle dynamically added delete buttons)
        $('#options-body').on('click', '.delete-option', function() {
            $(this).closest('.option-row').remove();
        });

        // Add event listener for checkbox change
        document.addEventListener('change', function(event) {
            const checkbox = event.target;
            if (checkbox.type === 'checkbox') {
                checkbox.previousElementSibling.value = checkbox.checked ? 'on' : 'off';
            }
        });

    };
</script>
@endsection