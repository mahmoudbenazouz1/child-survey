@extends('boilerplate::layout.index', [
'title' => __('boilerplate::meals.title'),
'subtitle' => __('boilerplate::meals.list.title'),
'breadcrumb' => [
__('boilerplate::meals.list.title') => 'boilerplate.meals.index'
]
])

@section('content')
<div class="row">
    <div class="col-12 mbl">
        <span class="float-right pb-3">
            <a href="{{ route("boilerplate.meals.create") }}" class="btn btn-primary">
                @lang('boilerplate::meals.create.title')
            </a>
        </span>
    </div>
</div>

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'meals']) @endcomponent

@endcomponent
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush