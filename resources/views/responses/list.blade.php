@extends('boilerplate::layout.index', [
'title' => __('boilerplate::surveys.responses.title'),
'subtitle' => __('boilerplate::surveys.responses.list.title'),
'breadcrumb' => [
__('boilerplate::surveys.responses.list.title') => 'boilerplate.responses.index'
]
])

@section('content')

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'responses']) @endcomponent

@endcomponent

@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush