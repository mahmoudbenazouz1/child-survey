@extends('boilerplate::layout.index', [
'title' => __('boilerplate::surveys.title'),
'subtitle' => __('boilerplate::surveys.list.title'),
'breadcrumb' => [
__('boilerplate::surveys.list.title') => 'boilerplate.surveys.index'
]
])

@section('content')
<div class="row">
    <div class="col-12 mbl">
        <span class="float-right pb-3">
            <a id="addSurveyButton" class="btn btn-primary">
                @lang('boilerplate::surveys.create.title')
            </a>
        </span>
    </div>
</div>



@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'surveys']) @endcomponent

@endcomponent

<div class="row">
    <form method="POST" action="{{ route('boilerplate.surveys.store') }}" autocomplete="off">
        @csrf
        <div class="modal fade" id="createSurveyModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('boilerplate::surveys.create.title')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @component('boilerplate::input', ['name' => 'name', 'label' => 'boilerplate::surveys.create.name'])@endcomponent
                        @component('boilerplate::select2', ['name' => 'status', 'label' => 'boilerplate::surveys.status', 'minimum-results-for-search' => '-1'])
                        <option value="1" @if (old('status', 1)=='1' ) selected="selected" @endif>@lang('boilerplate::surveys.active')</option>
                        <option value="0" @if (old('status')=='0' ) selected="selected" @endif>@lang('boilerplate::surveys.inactive')</option>
                        @endcomponent
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('boilerplate::surveys.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('boilerplate::surveys.save')</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form id="editForm" action="" method="POST">
        <div class="modal fade" id="editSurveyModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('boilerplate::surveys.edit.title')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        @component('boilerplate::input', ['name' => 'survey_name', 'label' => 'boilerplate::surveys.create.name', 'id' => 'surveyName' ])@endcomponent
                        @component('boilerplate::select2', ['name' => 'survey_status', 'label' => 'boilerplate::surveys.status', 'id' => 'status', 'minimum-results-for-search' => '-1'])
                        <option value="1">@lang('boilerplate::surveys.active')</option>
                        <option value="0">@lang('boilerplate::surveys.inactive')</option>
                        @endcomponent
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('boilerplate::surveys.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('boilerplate::surveys.save')</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // show add survey modal
        document.getElementById('addSurveyButton').addEventListener('click', function() {
            $('#createSurveyModal').modal('show')
        });
        // error ADD survey modal
        var name = <?php echo json_encode($errors->has(['name'])) ?>;
        var status = <?php echo json_encode($errors->has(['status_select'])) ?>;
        if (name || status) {
            $('#createSurveyModal').modal('show');
        }

        // add data to the edit modal
        $(document).on('click', '.edit-button', function() {
            var surveyId = $(this).data('id');
            $.ajax({
                url: "{{ route('boilerplate.surveys.show', ['survey' => '__surveyId__']) }}".replace('__surveyId__', surveyId),
                method: 'GET',
                success: function(data) {
                    $('#surveyName').val(data.name);
                    $('#status').val(data.status).trigger('change');
                    $('#editForm').attr('action', "{{ route('boilerplate.surveys.update', ['survey' => '__surveyId__']) }}".replace('__surveyId__', surveyId));
                },
                error: function(error) {
                    console.log(error)
                }
            });
        });


    });
</script>
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush