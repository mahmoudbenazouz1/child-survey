@extends('boilerplate::layout.index', [
'title' => __('boilerplate::surveys.title'),
'subtitle' => $question->survey->name . ': ' . $question->name,
'breadcrumb' => [
__('boilerplate::surveys.title') => 'boilerplate.surveys.index',
$question->survey->name,
$question->name,
]
])


@section('content')
<form method="POST" action="{{ route('boilerplate.questions.update',$question->id) }}" autocomplete="off" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route('boilerplate.surveys.edit',$question->survey->id) }}" class="btn btn-default" data-toggle="tooltip" title="{{$question->survey->name}}">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::surveys.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::surveys.edit_question'])
            @if($errors->any())
            <div class="alert alert-danger">
                @php
                $displayedErrors = [];
                @endphp
                @foreach ($errors->all() as $error)
                @if (!in_array($error, $displayedErrors) && strpos($error, 'name') !== 0)
                <div>{{ $error }}</div>
                @php
                $displayedErrors[] = $error;
                @endphp
                @endif
                @endforeach
            </div>
            @endif
            @component('boilerplate::input', ['name' => 'name', 'label' => 'boilerplate::surveys.edit.name', 'value'=>$question->name])@endcomponent
            <p><button type="button" class="btn btn-info mt-3" id="add-option-btn">@lang('boilerplate::surveys.add_option')</button></p>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="options" role="tabpanel" aria-labelledby="options-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="options-table">
                            <thead>
                                <tr>
                                    <th>@lang('boilerplate::surveys.upload_files')</th>
                                    <th>@lang('boilerplate::surveys.correct_option')</th>
                                    <th>@lang('boilerplate::surveys.option_name')</th>
                                    <th>@lang('boilerplate::surveys.action')</th>
                                </tr>
                            </thead>
                            <tbody id="options-body">
                                <!-- First row -->
                                @foreach($question->options as $key => $option)
                                <tr class="option-row">
                                    <td>
                                        <label for="file_{{ $key }}" id="label_file_{{ $key }}" style="display: none;">@lang('boilerplate::surveys.upload_file')</label>
                                        <input type="file" id="file_{{ $key }}" name="options[{{ $key }}][file]" style="display: none;">

                                        <div id="image-container_{{ $key }}" class="image-container">
                                            <img id="image-preview_{{ $key }}" src="{{ asset($option->image) }}" alt="Option Image" width="100">
                                            <button type="button" class="btn btn-danger remove-image" data-key="{{ $key }}">X</button>
                                            <input class="form-check-input" type="hidden" value="{{$option->id}}" name="options[{{ $key }}][old_file]">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="hidden" value="{{$option->id}}" name="options[{{ $key }}][old]">
                                            <input class="form-check-input" type="hidden" value="{{$option->correct_answer}}" name="options[{{ $key }}][correct]">
                                            <input class="form-check-input" type="checkbox" id="correct_{{ $key }}" name="options[{{ $key }}][correct]" {{ $option->correct_answer === 'on' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="correct_{{ $key }}">@lang('boilerplate::surveys.correct_option')</label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="option_name_{{ $key }}" name="options[{{ $key }}][option_name]" class="form-control" value="{{$option->text}}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-option">Delete</button>
                                    </td>
                                </tr>


                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endcomponent
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        // Counter for unique IDs
        let optionCounter = <?php echo json_encode(count($question->options)) ?>;

        // Function to add a new option row to the table
        function addOptionRow() {
            optionCounter++;

            let optionRow = `<tr class="option-row">
        <td>
            <label for="file_${optionCounter}">@lang('boilerplate::surveys.upload_file')</label>
            <input type="file" id="file_${optionCounter}" name="options[${optionCounter}][file]">
        </td>
        <td>
            <div class="form-check">
                <input class="form-check-input" type="hidden" value="off" name="options[${optionCounter}][correct]">
                <input class="form-check-input" type="checkbox" id="correct_${optionCounter}" name="options[${optionCounter}][correct]">
                <label class="form-check-label" for="correct_${optionCounter}">@lang('boilerplate::surveys.correct_option')</label>
            </div>
        </td>
        <td>
            <input type="text" id="option_name_${optionCounter}" name="options[${optionCounter}][option_name]" class="form-control">
        </td>
        <td>
            <button type="button" class="btn btn-danger delete-option">Delete</button>
        </td>
        </tr>
        `;

            $('#options-body').append(optionRow);
        }

        // Add option button click event
        $('#add-option-btn').click(function() {
            addOptionRow();
        });

        // Delete option button click event (delegated to handle dynamically added delete buttons)
        $('#options-body').on('click', '.delete-option', function() {
            $(this).closest('.option-row').remove();
        });

        // Add event listener for checkbox change
        document.addEventListener('change', function(event) {
            const checkbox = event.target;
            if (checkbox.type === 'checkbox') {
                checkbox.previousElementSibling.value = checkbox.checked ? 'on' : 'off';
            }
        });

        $('.remove-image').on('click', function() {
            var key = $(this).data('key');
            $('#image-container_' + key).remove(); // Remove the parent div
            $('#label_file_' + key).show(); // Show the label file input
            $('#file_' + key).show(); // Show the file input
            $('#file_' + key).val(''); // Clear the file input value
        });
    };
</script>
@endsection