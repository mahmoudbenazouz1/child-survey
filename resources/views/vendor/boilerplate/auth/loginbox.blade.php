
<div class="login-box">
    <div class="login-logo">
    <img src="{{ asset('assets/background/logo.png') }}" alt="Logo" style="max-width: 150px;">
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            {{ $slot }}
        </div>
    </div>
</div>
