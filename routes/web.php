<?php

use App\Http\Controllers\DoctorController;
use App\Http\Controllers\FrontOfficeController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\MealController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\ResponseController;
use App\Http\Controllers\SurveyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [FrontOfficeController::class, 'index'])->name('frontoffice.index');
Route::get('/front-games', [FrontOfficeController::class, 'games'])->name('frontoffice.games');
Route::get('/front-meals', [FrontOfficeController::class, 'meals'])->name('frontoffice.meals');
Route::get('surveys/{survey}/create', [FrontOfficeController::class, 'create'])->name('frontoffice.create');
Route::post('surveys/submissions/store', [FrontOfficeController::class, 'store'])->name('frontoffice.store');

Route::group([
    'domain' => config('boilerplate.app.domain', ''),
    'middleware' => ['web', 'boilerplate.locale'],
    'as' => 'boilerplate.',
], function () {

    //all routes must be protected by auth
    Route::group(['middleware' => ['auth', 'boilerplate.emailverified']], function () {

        // Admin routes
        Route::group(['middleware' => ['role:admin']], function () {
            Route::get('hospitals', [HospitalController::class, 'index'])->name('hospitals.index');
            Route::post('hospitals', [HospitalController::class, 'store'])->name('hospitals.store');
            Route::get('hospitals/create', [HospitalController::class, 'create'])->name('hospitals.create');
            Route::get('hospitals/{hospital}/edit', [HospitalController::class, 'edit'])->name('hospitals.edit');
            Route::put('hospitals/{hospital}/update', [HospitalController::class, 'update'])->name('hospitals.update');
        });

        // Manager routes
        Route::group(['middleware' => ['role:manager']], function () {
            Route::get('doctors', [DoctorController::class, 'index'])->name('doctors.index');
            Route::post('doctors', [DoctorController::class, 'store'])->name('doctors.store');
            Route::get('doctors/create', [DoctorController::class, 'create'])->name('doctors.create');
            Route::get('doctors/{doctor}/edit', [DoctorController::class, 'edit'])->name('doctors.edit');
            Route::put('doctors/{doctor}/update', [DoctorController::class, 'update'])->name('doctors.update');
        });
        // managers doctors routes
        Route::group(['middleware' => ['role:manager|doctor']], function () {
            Route::get('surveys', [SurveyController::class, 'index'])->name('surveys.index');
            Route::post('surveys', [SurveyController::class, 'store'])->name('surveys.store');
            Route::get('surveys/{survey}/show', [SurveyController::class, 'show'])->name('surveys.show');
            Route::get('surveys/{survey}/edit', [SurveyController::class, 'edit'])->name('surveys.edit');
            Route::put('surveys/{survey}/update', [SurveyController::class, 'update'])->name('surveys.update');
            Route::delete('surveys/{survey}/destroy', [SurveyController::class, 'destroy'])->name('surveys.destroy');

            Route::get('responses', [ResponseController::class, 'index'])->name('responses.index');
            Route::get('responses/{survey}/edit', [ResponseController::class, 'edit'])->name('responses.edit');
            Route::delete('responses/{survey}/destroy', [ResponseController::class, 'destroy'])->name('responses.destroy');

            Route::post('surveys/{survey}/questions/store', [QuestionController::class, 'store'])->name('questions.store');
            Route::get('surveys/questions/{question}/edit', [QuestionController::class, 'edit'])->name('questions.edit');
            Route::put('surveys/questions/{question}/update', [QuestionController::class, 'update'])->name('questions.update');
            Route::delete('surveys/questions/{question}/destroy', [QuestionController::class, 'destroy'])->name('questions.destroy');

            Route::get('meals', [MealController::class, 'index'])->name('meals.index');
            Route::post('meals', [MealController::class, 'store'])->name('meals.store');
            Route::get('meals/create', [MealController::class, 'create'])->name('meals.create');
            Route::get('meals/{meal}/edit', [MealController::class, 'edit'])->name('meals.edit');
            Route::put('meals/{meal}/update', [MealController::class, 'update'])->name('meals.update');
            Route::delete('meals/{meal}/destroy', [MealController::class, 'destroy'])->name('meals.destroy');
        });
    });
});
